<?php

namespace OXIT\SeznamMaps;

/**
 * Class SeznamMapsExtension
 *
 * @author Lukas Novotny <lukas.novotny@oxit.cz>
 */
final class SeznamMapsExtension extends \Nette\DI\CompilerExtension
{

	/**
	 * Výchozí konfigurační hodnoty.
	 * @var array
	 */
	private $defaults = [
		"mapCenter" => [

			'x' => 14.3681811,
			'y' => 50.083517,
		],
		"mapZoom" => 14
	];


	public function loadConfiguration()
	{
		$this->validateConfig($this->defaults);

		$builder = $this->getContainerBuilder();

		$this->compiler->loadDefinitionsFromConfig(
			$this->loadFromFile(__DIR__ . '/config.neon')['services']
		);

		$builder->getDefinition('mapFactory')
			->getResultDefinition()->addSetup('setSettings', [$this->config]);

		$builder->getDefinition('mapPickerFactory')
			->getResultDefinition()->addSetup('setSettings', [$this->config]);
	}
}