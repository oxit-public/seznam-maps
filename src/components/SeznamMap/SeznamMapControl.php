<?php

namespace OXIT\SeznamMaps;


use Nette\Application\UI\Control;
use Nette\Localization\ITranslator;

class SeznamMapControl extends Control
{
	/** @var ITranslator */
	public $translator;

	protected $coords = [];

	/** @var array */
	protected $mapCenter;

	/** @var int */
	protected $mapZoom;

	public function setSettings($config)
	{
		$this->mapCenter = $config['mapCenter'];
		$this->mapZoom = $config['mapZoom'];
	}

	public function render()
	{
		$this->template->render(__DIR__ . DIRECTORY_SEPARATOR . 'SeznamMapControl.latte');
	}

	public function renderJs()
	{
		if (!empty($this->coords)) {
			$this->template->coords = $this->coords;
		}

		$this->template->mapCenter = $this->mapCenter;
		$this->template->mapZoom = $this->mapZoom;

		$this->template->render(__DIR__ . DIRECTORY_SEPARATOR . 'SeznamMapControlJs.latte');
	}

	/** @param ITranslator $translator */
	public function setTranslator(ITranslator $translator): void
	{
		$this->translator = $translator;
	}

	/** @param array $coords */
	public function setCoords(array $coords): void
	{
		$this->coords = $coords;
	}

	/** @param array $coords */
	public function setMapCenter(array $mapCenter): void
	{
		$this->mapCenter = $mapCenter;
	}

	/** @param int $zoom */
	public function setMapZoom(int $zoom): void
	{
		$this->mapZoom = $zoom;
	}
}

interface ISeznamMapControlFactory
{
	/**
	 * @return SeznamMapControl
	 */
	public function create();

}
