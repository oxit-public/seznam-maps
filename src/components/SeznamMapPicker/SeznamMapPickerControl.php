<?php

namespace OXIT\SeznamMaps;


use Nette\Application\UI\Presenter;

class SeznamMapPickerControl extends SeznamMapControl
{
	/** @var string */
	protected $inputX;

	/** @var string */
	protected $inputY;

	public function render()
	{
		$this->template->render(__DIR__ . DIRECTORY_SEPARATOR . 'SeznamMapPickerControl.latte');
	}

	public function renderJs()
	{
		if (!empty($this->coords)) {
			$this->template->coords = $this->coords;
		}

		$this->template->mapCenter = $this->mapCenter;
		$this->template->mapZoom = $this->mapZoom;

		$this->template->inputX = $this->inputX;
		$this->template->inputY = $this->inputY;

		$this->template->render(__DIR__ . DIRECTORY_SEPARATOR . 'SeznamMapPickerControlJs.latte');
	}

	/**
	 * @param string $inputX
	 */
	public function setInputX(string $inputX): void
	{
		$this->inputX = $inputX;
	}

	/**
	 * @param string $inputY
	 */
	public function setInputY(string $inputY): void
	{
		$this->inputY = $inputY;
	}

	public function handleUpdateCoords()
	{
		$this->redrawControl('control');
	}

	public function handleLoadCoords()
	{

	}


}

interface ISeznamMapPickerControlFactory
{
	/**
	 * @return SeznamMapPickerControl
	 */
	public function create();

}
