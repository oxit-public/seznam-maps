# seznam-maps

Seznam API for Nette

**INSTALACE**

do config.neon přidat extensions + jde nastavit default středu a zoomu

```
extensions:
    senzamMap: OXIT\SeznamMaps\SeznamMapsExtension

senzamMap:
	mapZoom: 14
	mapCenter:
		x: 14.3681811
		y: 50.083517
```

**POUŽITÍ**

v presenteru injectnout factory:

```
/** @var ISeznamMapControlFactory  @inject */
public $seznamMapControl;
```

a vytvořit komponentu
```
protected function createComponentSeznamMap()
{
    $control = $this->seznamMapControl->create();

	$control->setTranslator($this->translator);

	if ($this->consultant->coords_x && $this->consultant->coords_y) {
		$control->setCoords([['x' => $this->consultant->coords_x, 'y' => $this->consultant->coords_y]]); // nastaveni libovolneho poctu bodu na mape, vstup pole polí, kde vnitřní pole má indexy x,y

		$control->setMapCenter(['x' => 10.4444, 'y' => 50.2222]); // nastavení středu vstup pole s indexy x,z
	}

	return $control;
}
```

	
v latte:
v HTML hlavičce includovat cesta z základním JS souboru 
```
{include OXIT\SeznamMaps\SeznamMapJsBuilder::FILE}
```
a poté zavolat komponentu
```
{control seznamMap}
```